package br.com.lead.collector.repositories;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
    Iterable<Produto> findAllByNome(String nome);
    Iterable<Produto> findAllByDescricaoContains(String descricao);
}
