package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registrarProduto(@RequestBody Produto produto){
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> exibirTodosProdutos(@RequestParam(name ="nomeProduto", required = false) String nome,
                                                 @RequestParam(name = "descricaoProduto", required = false) String descricao){
        if(nome != null){
            Iterable<Produto> produtos = produtoService.buscarTodosPorNome(nome);
            return produtos;
        }else{
            if(descricao != null){
                Iterable<Produto> produtos = produtoService.buscarTodosPorDescricao(descricao);
                return produtos;
            }
        }
        Iterable<Produto> produtos = produtoService.buscarTodosProdutos();
        return produtos;
    }

    @GetMapping("/{id}")
    public Produto bucarPorId(@PathVariable(name = "id") int id){
        try{
            Produto produto = produtoService.buscarPorId(id);
            return produto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") int id, @RequestBody Produto produto){
        try{
            Produto produtoobjeto = produtoService.atualizarProduto(id, produto);
            return produtoobjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    //se funciona
    @PatchMapping("/{id}")
    public Produto atualizarProdutoParcial(@PathVariable(name = "id") int id, @RequestBody Produto produto){
        try{
            Produto produtoobjeto = produtoService.atualizarProdutoParcial(id, produto);
            return produtoobjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable int id){
        try{
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }




}
