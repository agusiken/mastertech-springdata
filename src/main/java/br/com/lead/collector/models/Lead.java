package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a 100 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Não pode ser só espaço")
    private String nome;


    @Email(message = "O formato do email é invalido")
    @NotNull(message = "Email não pode ser nulo")
    private String email;

    @Null(message = "O campo data não é preenchido")
    private LocalDate data;

    @NotNull(message = "Tipo Lead não pode ser vazio")
    private TipoLeadEnum tipoLead;

    public Lead(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
